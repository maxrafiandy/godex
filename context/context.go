package gode

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/gorilla/schema"
	"github.com/labstack/echo"
	"gorm.io/gorm"

	vald "github.com/go-ozzo/ozzo-validation"
)

type ContextIndex uint

const (
	SessionContext ContextIndex = 10000
	ParamContext   ContextIndex = 10001
)

type Session interface {
	SetSession(Session) error
}

type Context struct {
	echo.Context
	_session Session
	_param   Param
}

type ContextModel interface {
	New(ctx *Context) ContextModel
	First(args ...interface{}) ContextModel
	Unscoped(args ...interface{}) ContextModel
	Where(query string, args ...interface{}) ContextModel
	Join(query string, args ...interface{}) ContextModel
	Preload(relation string, args ...interface{}) ContextModel
	Page(args ...interface{}) PageData
	IsEmpty() bool
	HasError() bool
}

type PageData struct {
	Items        interface{} `json:"items"`
	Keyword      string      `json:"keyword"`
	ItemsPerPage int         `json:"items_per_page"`
	TotalItems   int64       `json:"total_items"`
	Page         int         `json:"page"`
	TotalPage    int64       `json:"total_page"`
	Error        error       `json:"error,omitempty"`
}

type Range struct {
	FilterBy string
	Start    string
	End      string
}

type Param struct {
	Keyword      string
	SearchBy     []string
	ItemsPerPage int
	Page         int
	OrderBy      string
	OrderMethod  string
	Range        Range
}

type Response struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data,omitempty"`
}

func NewContext(c echo.Context) *Context {
	return c.(*Context)
}

// FormData parse the incoming POST body into "form" struct
// handle application/json and application/x-www-form-urlencoded
// and multipart/form-data
func (c *Context) FormData(form interface{}) error {
	var content string = c.Request().Header.Get("content-type")

	if strings.Contains(content, "application/json") {
		decoder := json.NewDecoder(c.Request().Body)
		decoder.Decode(form)
		return nil
	} else if strings.Contains(content, "application/x-www-form-urlencoded") {
		c.Request().ParseForm()
		decoder := schema.NewDecoder()
		decoder.Decode(form, c.Request().Form)
		return nil
	} else if strings.Contains(content, "multipart/form-data") {
		c.Request().ParseMultipartForm(1024 << 10)
		decoder := schema.NewDecoder()
		decoder.Decode(form, c.Request().Form)
		return nil
	}
	var err = vald.Errors{"error": errors.New("content-type data tidak sesuai")}
	return err
}

func (c *Context) FormValidatable(form vald.Validatable) (err error) {
	err = c.FormData(form)
	if err == nil {
		err = form.Validate()
	}
	return err
}

func (ctx *Context) GetParam() Param {
	return ctx._param
}

func (ctx *Context) SetParam(param Param) {
	ctx._param = param
}

func (ctx *Context) GetSession() Session {
	return ctx._session
}

func (ctx *Context) SetSession(_session Session, typeOf reflect.Type) (err error) {
	if reflect.TypeOf(_session) == typeOf {
		ctx._session = _session
	} else {
		err = errors.New("tipe data session tidak valid")
	}
	return
}

// Pagination set offset and limit of query.
// Default value of limit is 10, and offset of page 1.
// Note: dateColumn is an optional parameter and the function
// only use dateColumn[0]. Call this function before PageResult
func Paginate(_db *gorm.DB, query Param, dest interface{}) PageData {
	var (
		withLike bool = false
		keyword  string
		limit    int = 10
		page     int = 0
		between  string
		gs       *gorm.DB        = _db.Session(&gorm.Session{NewDB: true})
		stm      *gorm.Statement = &gorm.Statement{DB: _db}
	)

	if query.ItemsPerPage > 0 {
		limit = query.ItemsPerPage
	}

	if len(query.SearchBy) > 0 && len(query.Keyword) > 0 {
		for _, column := range query.SearchBy {
			keyword = fmt.Sprintf("lower(%s) like ?", column)
			gs = gs.Or(keyword, fmt.Sprintf("%%%s%%", strings.ToLower(query.Keyword)))
			withLike = true
		}
	}

	if withLike {
		_db = _db.Where(gs)
	}

	stm.Parse(dest)

	newContext, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	gs = _db.WithContext(newContext)

	if gs.Migrator().HasColumn(dest, query.OrderBy) {
		if strings.ToUpper(query.OrderMethod) == "DESC" {
			_db = _db.Order(stm.Schema.Table + "." + query.OrderBy + " DESC")
		} else {
			_db = _db.Order(stm.Schema.Table + "." + query.OrderBy + " ASC")
		}
	}

	_db = _db.Limit(limit)
	page = (query.Page - 1) * limit
	_db = _db.Offset(page)

	if query.Range.FilterBy != "" {
		switch {
		case len(query.Range.Start) > 0 && len(query.Range.End) > 0:
			between = fmt.Sprintf("date(%s) between ? and ?", query.Range.FilterBy)
			_db = _db.Where(between, query.Range.Start, query.Range.End)
		case len(query.Range.Start) > 0:
			between = fmt.Sprintf("date(%s) = ?", query.Range.FilterBy)
			_db = _db.Where(between, query.Range.Start)
		case len(query.Range.End) > 0:
			between = fmt.Sprintf("date(%s) between ? and ?", query.Range.FilterBy)
			_db = _db.Where(between, time.Now().Format("2006-01-02"), query.Range.End)
		}
	}

	_db = _db.Find(dest)
	if _db.Error != nil {
		return PageData{
			Error:        _db.Error,
			Items:        []string{},
			Keyword:      query.Keyword,
			ItemsPerPage: limit,
			TotalPage:    1,
			Page:         1,
		}
	}

	var result = paginate(_db, dest, query)
	result.Error = _db.Error
	return result
}

// PageResult handle detail of BuildQuery and returns PaginationResult.
// This should called after Pagination() function to generate offset limit.
// Warning: must supply executed gorm.DB object () ex: PageResult(db.Find(&users), urlQuery)
func paginate(_db *gorm.DB, dest interface{}, query Param) PageData {
	var (
		count int64
		limit int = 10
		pager int = 0
		page  PageData
	)

	if len(query.OrderBy) > 0 {
		_db = _db.Group(query.OrderBy)
	}

	_db.Offset(-1).Count(&count)

	if query.ItemsPerPage > 0 {
		limit = query.ItemsPerPage
		if limit > 100 {
			limit = 100
		}
	}

	if query.Page == 0 {
		query.Page = 1
	}

	pager = (query.Page - 1) * limit
	page.Items = dest
	page.Keyword = query.Keyword
	page.ItemsPerPage = limit
	page.Page = pager/limit + 1
	page.TotalItems = count
	page.TotalPage = int64(math.Ceil(float64(count) / float64(limit)))

	if page.TotalPage == 0 {
		page.TotalPage = 1
	}

	return page
}

func (ctx *Context) Success(payload interface{}) error {

	return ctx.JSON(http.StatusOK,
		Response{
			Status: "berhasil",
			Data:   payload,
		})
}

func (ctx *Context) NotFound(err error) error {

	return ctx.JSON(http.StatusNotFound,
		Response{
			Status: "record tidak ditemukan",
			Data: vald.Errors{
				"message": err,
			},
		})
}

func (ctx *Context) BadRequest(err error) error {
	return ctx.JSON(http.StatusBadRequest,
		Response{
			Status: "input belum sesuai",
			Data:   err,
		})
}

func (ctx *Context) Conflict(err error) error {
	return ctx.JSON(
		http.StatusBadRequest,
		Response{
			Status: "data duplikat",
			Data: vald.Errors{
				"message": err,
			},
		})
}

func (ctx *Context) Unauthorized(err error) error {
	return ctx.JSON(
		http.StatusUnauthorized,
		Response{
			Status: "user tidak teridentifikasi",
			Data: vald.Errors{
				"message": err,
			},
		})
}

func (ctx *Context) Forbidden(err error) error {
	return ctx.JSON(
		http.StatusForbidden,
		Response{
			Status: "akses dibatasi",
			Data: vald.Errors{
				"message": err,
			},
		},
	)
}

func (ctx *Context) ServerError(err error) error {
	var response Response

	response.Status = "internal server error"

	switch os.Getenv("DEBUG") {
	case "1", "true":
		response.Data = vald.Errors{"message": err}
	default:
		response.Data = vald.Errors{"message": errors.New("terjadi kendala teknis")}
	}

	return ctx.JSON(http.StatusForbidden, response)
}
