package config

import (
	"fmt"
	"os"

	"github.com/go-redis/redis"
)

var (
	cache *redis.Client
)

func ConnectRedis() (err error) {
	var (
		host = os.Getenv("REDIS_HOST")
		port = os.Getenv("REDIS_PORT")
		pswd = os.Getenv("REDIS_PSWD")
	)

	opt := redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: pswd,
		DB:       0,
	}
	cache = redis.NewClient(&opt)
	_, err = cache.Ping().Result()
	return err
}

func GetCache() *redis.Client {
	return cache
}
