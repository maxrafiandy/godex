package config

import "strconv"

type JwtConfiguration struct {
	SigningKey    []byte
	JwtExpiration uint
}

func JwtConfig() *JwtConfiguration {

	var (
		// signKey = os.Getenv("SIGNING_KEY")
		// expired = os.Getenv("JWT_EXPIRATION")
		signKey string = "eco-hub"
		expired string = "15"
	)

	exp, _ := strconv.ParseUint(expired, 10, 32)

	return &JwtConfiguration{
		SigningKey:    []byte(signKey),
		JwtExpiration: uint(exp),
	}
}
