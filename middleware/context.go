package middleware

import (
	"gitlab/godex/config"
	ct "gitlab/godex/context"
	"gitlab/godex/model"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	vald "github.com/go-ozzo/ozzo-validation"
	"github.com/labstack/echo"
)

func chain(c echo.Context) (*ct.Context, error) {
	var ctx = &ct.Context{Context: c}

	page, _ := strconv.ParseInt(c.Request().URL.Query().Get("page"), config.IntBase, config.IntBitSize)
	limit, _ := strconv.ParseInt(c.Request().URL.Query().Get("items_per_page"), config.IntBase, config.IntBitSize)

	var searchArr []string
	searchBy := c.Request().URL.Query().Get("search_by")
	if len(searchBy) > 0 {
		searchArr = strings.Split(searchBy, ",")
	}

	ctx.SetParam(ct.Param{
		Keyword:      c.Request().URL.Query().Get("keyword"),
		SearchBy:     searchArr,
		Page:         int(page),
		ItemsPerPage: int(limit),
	})

	if err := ctx.SetSession(
		ctx.Request().Context().Value(ct.SessionContext).(*model.Session),
		reflect.TypeOf(&model.Session{})); err != nil {
		return nil, err
	}

	return ctx, nil
}

func Context(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx, err := chain(c)

		if err != nil {
			return c.JSON(
				http.StatusInternalServerError,
				ct.Response{
					Status: "internal server error",
					Data: vald.Errors{
						"message": err,
					},
				})
		}

		return next(ctx)
	}
}
