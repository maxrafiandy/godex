package middleware

import (
	"context"
	_context "gitlab/godex/context"
	"gitlab/godex/model"

	"github.com/labstack/echo"
)

func Session(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var r = c.Request()

		c.SetRequest(
			r.WithContext(context.WithValue(
				r.Context(),
				_context.SessionContext,
				&model.Session{
					UserID: 2,
					RoleID: 1,
					ShopID: 6,
				}),
			))

		return next(c)
	}
}
