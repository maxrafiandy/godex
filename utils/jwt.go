package utils

import (
	"errors"
	"gitlab/godex/config"
	"time"

	"github.com/golang-jwt/jwt"
)

var jwtConfig = config.JwtConfig()

type Claims struct {
	UserID uint `json:"user_id"`
	RoleID uint `json:"role_id"`
	ShopID uint `json:"shop_id"`
	jwt.StandardClaims
}

func GetToken(userId uint, roleId uint, shopId uint) (string, error) {
	claims := &Claims{
		UserID: userId,
		RoleID: roleId,
		ShopID: shopId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(jwtConfig.JwtExpiration) * time.Minute).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtConfig.SigningKey)
	return tokenString, err
}

func VerifyToken(token string) (*Claims, error) {
	keyFunc := func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New("Token Invalid")
		}
		return jwtConfig.SigningKey, nil
	}

	jwtToken, err := jwt.ParseWithClaims(token, &Claims{}, keyFunc)
	if err != nil {
		return nil, err
	}

	return jwtToken.Claims.(*Claims), err

}
