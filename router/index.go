package router

import (
	"gitlab/godex/controller"
	"gitlab/godex/middleware"

	"github.com/labstack/echo"
)

const base = ""

func Router() (e *echo.Echo) {
	e = echo.New()

	root := e.Group(base, middleware.Session, middleware.Context)

	user := root.Group("/users")
	user.GET(base, controller.GetAllUser)
	user.POST("/create", controller.CreateUser)
	user.PUT("/:user_id/edit", controller.UpdateUser)
	user.PATCH("/:user_id/change-role", controller.PatchUserRole)
	user.GET("/my-profile", controller.GetUserBySession)
	user.GET("/:user_id/profile", controller.GetUserByID)
	user.DELETE("/:user_id/delete", controller.DeleteUserByID)

	return e
}
