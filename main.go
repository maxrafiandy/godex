package main

import (
	"fmt"
	"gitlab/godex/config"
	"gitlab/godex/router"
	"log"

	"github.com/joho/godotenv"
)

func main() {
	var err error

	_echo := router.Router()
	if err = godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	if err = config.ConnectDB(); err != nil {
		log.Fatal(err)
	}

	if err = config.ConnectRedis(); err != nil {
		log.Fatal(err)
	}

	appPort := fmt.Sprintf(":%d", 8088)
	_echo.Logger.Fatal(_echo.Start(appPort))
}
