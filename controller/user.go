package controller

import (
	"errors"
	"gitlab/godex/model"

	"github.com/labstack/echo"
)

func CreateUser(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
		form Userf
	)

	if err = ctx.FormValidatable(&form); err != nil {
		return ctx.BadRequest(err)
	}

	u := user.(*model.User)
	u.Name = form.Name
	u.RoleID = form.RoleID
	u.ShopID = form.ShopID

	if err = u.Create(); err != nil {
		return ctx.ServerError(err)
	}

	return ctx.Success(user)
}

func UpdateUser(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
		form Userf
	)

	// get form data, validate automatically
	if err = ctx.FormValidatable(&form); err != nil {
		return ctx.BadRequest(err)
	}

	user.First("id = ?", ctx.Param("user_id"))

	if user.IsEmpty() {
		return ctx.NotFound(errors.New("user tidak ditemukan"))
	}

	u := user.(*model.User)
	u.Name = form.Name
	u.RoleID = form.RoleID
	u.ShopID = form.ShopID

	if err = u.Save(); err != nil {
		return ctx.ServerError(err)
	}

	return ctx.Success(user)
}

func PatchUserRole(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
		form RoleIDf
	)

	if err = ctx.FormValidatable(&form); err != nil {
		return ctx.BadRequest(err)
	}

	user.First("id = ?", ctx.Param("user_id"))
	if user.IsEmpty() {
		return ctx.NotFound(errors.New("user tidak ditemukan"))
	}

	u := user.(*model.User)
	u.RoleID = form.RoleID

	if err = u.Save(); err != nil {
		return ctx.ServerError(err)
	}

	return ctx.Success(user)
}

func GetUserBySession(c echo.Context) (err error) {
	var (
		ctx     = newc(c)
		user    = newm(&model.User{}, ctx)
		session = ctx.GetSession().(*model.Session)
	)

	data := user.First("id = ?", session.UserID)
	return ctx.Success(data)
}

func GetUserByID(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
	)

	user.First("id = ?", ctx.Param("user_id"))
	if user.(*model.User).IsEmpty() {
		return ctx.NotFound(errors.New("user tidak ditemukan"))
	}

	return ctx.Success(user)
}

func GetAllUser(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
	)

	users := user.Page()
	return ctx.Success(users)
}

func DeleteUserByID(c echo.Context) (err error) {
	var (
		ctx  = newc(c)
		user = newm(&model.User{}, ctx)
	)

	user.First("id = ?", ctx.Param("user_id"))
	if user.IsEmpty() {
		return ctx.NotFound(errors.New("user tidak ditemukan"))
	}

	u := user.(*model.User)
	u.Delete()
	if user.HasError() {
		return ctx.ServerError(u.Error)
	}

	return ctx.Success(user)
}
