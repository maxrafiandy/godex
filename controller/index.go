package controller

import (
	ct "gitlab/godex/context"

	vald "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/labstack/echo"
)

func newc(c echo.Context) *ct.Context {
	return ct.NewContext(c)
}

func newm(dest ct.ContextModel, c *ct.Context) ct.ContextModel {
	return dest.New(c)
}

type Userf struct {
	Namef
	ShopIDf
	RoleIDf
}

func (form Userf) Validate() error {
	return vald.ValidateStruct(&form,
		vald.Field(&form.Name),
		vald.Field(&form.ShopID),
		vald.Field(&form.RoleIDf),
	)
}

type Namef struct {
	Name string `json:"name"`
}

func (form Namef) Validate() error {
	return vald.ValidateStruct(&form,
		vald.Field(&form.Name, vald.Required, is.Alpha),
	)
}

type ShopIDf struct {
	ShopID uint `json:"shop_id"`
}

func (form ShopIDf) Validate() error {
	return vald.ValidateStruct(&form,
		vald.Field(&form.ShopID, vald.Required),
	)
}

type RoleIDf struct {
	RoleID uint `json:"role_id"`
}

func (form RoleIDf) Validate() error {
	return vald.ValidateStruct(&form,
		vald.Field(&form.RoleID, vald.Required),
	)
}
