package model

import (
	ct "gitlab/godex/context"

	"gorm.io/gorm"
)

type User struct {
	Model
	Name   string `json:"name"`
	ShopID uint   `json:"shop_id"`
	RoleID uint   `json:"role_id"`
}

type Users []User

func (m *User) New(ctx *ct.Context) ct.ContextModel {
	m.SetContext(ctx)
	return m
}

func (m *User) Join(query string, args ...interface{}) ct.ContextModel {
	return m.join(m, query, args...)
}

func (m *User) Preload(relation string, args ...interface{}) ct.ContextModel {
	return m.preload(m, relation, args...)
}

func (m *User) Page(args ...interface{}) ct.PageData {
	var users Users
	return m.page(&users, args...)
}

func (m *User) First(args ...interface{}) ct.ContextModel {
	return m.first(m, args...)
}

func (m *User) Unscoped(args ...interface{}) ct.ContextModel {
	return m.unscoped(m, args...)
}

func (m *User) Where(query string, args ...interface{}) ct.ContextModel {
	return m.where(m, query, args...)
}

// Make your own funcs here
func (m *User) Create() error {
	return db().Transaction(func(tx *gorm.DB) (err error) {
		return tx.Create(m).Error
	})
}

func (m *User) Save() error {
	return db().Transaction(func(tx *gorm.DB) (err error) {
		return tx.Save(m).Error
	})
}

func (m *User) Delete() error {
	return db().Transaction(func(tx *gorm.DB) (err error) {
		return tx.Delete(m).Error
	})
}
