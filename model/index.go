package model

import (
	"gitlab/godex/config"
	ct "gitlab/godex/context"
	"time"

	"gorm.io/gorm"
)

type join struct {
	query string
	args  []interface{}
}

type Model struct {
	ID        uint           `gorm:"primarykey" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`
	Error     error          `gorm:"-" json:"error,omitempty"`
	preds     []join
	joins     []join
	wheres    []join
	ctx       *ct.Context
}

func db() *gorm.DB {
	return config.CloneDB()
}

func (m *Model) SetContext(ctx *ct.Context) {
	m.ctx = ctx
}

func (m *Model) first(dest ct.ContextModel, args ...interface{}) ct.ContextModel {
	var db *gorm.DB = db()

	if len(args) == 1 {
		db = db.Where(args[0].(string))
	} else if len(args) > 1 {
		db = db.Where(args[0].(string), args[1:]...)
	}

	db = m.merge(db)
	m.Error = db.First(dest).Error
	return dest
}

func (m *Model) unscoped(dest ct.ContextModel, args ...interface{}) ct.ContextModel {
	var db *gorm.DB = db()

	if len(args) == 1 {
		db = db.Unscoped().Where(args[0].(string))
	} else if len(args) > 1 {
		db = db.Unscoped().Where(args[0].(string), args[1:]...)
	}

	m.Error = db.First(dest).Error
	return dest
}

func (m *Model) page(dest interface{}, args ...interface{}) ct.PageData {
	var db *gorm.DB = db()

	if len(args) == 1 {
		db = db.Where(args[0].(string))
	} else if len(args) > 1 {
		db = db.Where(args[0].(string), args[1:]...)
	}

	db = m.merge(db)
	return ct.Paginate(db, m.ctx.GetParam(), dest)
}

func (m *Model) preload(dest ct.ContextModel, relation string, args ...interface{}) ct.ContextModel {
	m.preds = append(m.preds, join{query: relation, args: args})
	return dest
}

func (m *Model) where(dest ct.ContextModel, query string, args ...interface{}) ct.ContextModel {
	m.wheres = append(m.wheres, join{query: query, args: args})
	return dest
}

func (m *Model) join(dest ct.ContextModel, query string, args ...interface{}) ct.ContextModel {
	m.joins = append(m.joins, join{query: query, args: args})
	return dest
}

func (m *Model) merge(db *gorm.DB) *gorm.DB {
	return m.mergeWhere(m.mergeJoin(m.mergePreload(db)))
}

func (m *Model) mergePreload(db *gorm.DB) *gorm.DB {
	for _, p := range m.preds {
		db = db.Preload(p.query, p.args...)
	}
	return db
}

func (m *Model) mergeJoin(db *gorm.DB) *gorm.DB {
	for _, j := range m.joins {
		db = db.Joins(j.query, j.args...)
	}
	return db
}

func (m *Model) mergeWhere(db *gorm.DB) *gorm.DB {
	for _, w := range m.wheres {
		db = db.Where(w.query, w.args...)
	}
	return db
}

func (m *Model) IsEmpty() bool {
	return m.ID == 0
}

func (m *Model) HasError() bool {
	return m.Error != nil
}
