package model

import (
	"errors"
	"reflect"

	_context "gitlab/godex/context"
)

type Session struct {
	UserID uint
	RoleID uint
	ShopID uint
}

func (s *Session) SetSession(_session _context.Session) (err error) {
	if reflect.TypeOf(_session) == reflect.TypeOf(s) {
		s = _session.(*Session)
	} else {
		err = errors.New("tipe data session tidak valid")
	}
	return
}
